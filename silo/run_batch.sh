#!/bin/bash
#echo 65536 >| /proc/sys/vm/nr_hugepages
echo never >| /sys/kernel/mm/transparent_hugepage/enabled

#./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 1 --scale-factor 1 --runtime 60 --numa-memory 112G >> batch/1t_1w
#./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 2 --scale-factor 2 --runtime 60 --numa-memory 112G >> batch/2t_2w
#./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 4 --scale-factor 4 --runtime 60 --numa-memory 112G >> batch/4t_4w
#./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 8 --scale-factor 8 --runtime 60 --numa-memory 112G >> batch/8t_8w
#./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 16 --scale-factor 16 --runtime 60 --numa-memory 112G >> batch/16t_16w
#./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 24 --scale-factor 24 --runtime 60 --numa-memory 112G >> batch/24t_24w

echo 1500000 >| /proc/sys/vm/nr_hugepages
#./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 1 --scale-factor 8640 --runtime 60 --numa-memory 1400G >> batch/1t_8640w
./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 2 --scale-factor 8640 --runtime 60 --numa-memory 1400G >> batch/2t_8640w
./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 4 --scale-factor 8640 --runtime 60 --numa-memory 1400G >> batch/4t_8640w
./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 8 --scale-factor 8640 --runtime 60 --numa-memory 1400G >> batch/8t_8640w
./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 16 --scale-factor 8640 --runtime 60 --numa-memory 1400G >> batch/16t_8640w
./out-perf.masstree/benchmarks/dbtest --verbose --bench tpcc --num-threads 24 --scale-factor 8640 --runtime 60 --numa-memory 1400G >> batch/24t_8640w


